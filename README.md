![moteur](moteur.png)

# Comment fonctionne un moteur de recherche ?

Un moteur de recherche est un ensemble de logiciels parcourant le Web puis indexant
automatiquement les pages visitées. Quatre étapes sont indispensables à son fonctionnement :
+ la collecte d’informations (ou crawl) grâce à des robots (ou spiders ou encore crawlers) ;
+ l’indexation des données collectées et la constitution d’une base de données de documents
nommée « index » ;
+ le traitement des requêtes, avec tout particulièrement un système d’interrogation de
l’index et de classement des résultats en fonction de critères de pertinence suite à la
saisie de mots-clés par l’utilisateur ;
+ la restitution des résultats identifiés, dans ce que l’on appelle communément des SERP
(Search Engine Result Pages) ou pages de résultats, le plus souvent présentées sous la
forme d’une liste de dix liens affichés les uns en dessous des autres.

Les pages de résultats des moteurs de recherche affichent deux principaux types de contenu : 
+ les liens « organiques » ou « naturels », obtenus grâce au crawl du Web 
+ et les liens sponsorisés, ou liens commerciaux.


# C'est quoi le SEO ?

Le SEO est l’acronyme de Search Engine Optimization.

En français, c’est l’optimisation pour les moteurs de recherche.

Il faut savoir qu’en France, les référenceurs travaillent presque uniquement pour positionner des pages sur Google.

Tout les internautes français l’utilisent. Google détenait 91,71% de part de marché en France en Janvier 2018.

Sa position est donc hégémonique et l’optimisation pour les autres moteurs de recherche (Bing 5,12% et Yahoo 1,69%) est tout à fait négligeable.

Dans d’autres pays, Google est loin d’être le moteur le plus utilisé.

Même si dans le monde, il reste le moteur de recherche de prédilection (92% de parts de marché) il n’est presque pas utilisé en Chine où le moteur Baidu est leader avec 64% de de part de marché. On peut également citer la Russie, où Yandex (54%) et Google (42%) se partagent le marché.

# Améliorer sa visibilité

Il existe plus de 200 critères permettant d’améliorer la visibilité d’un site sur un moteurs de recherche.

Mais 3 critères sont à particulièrement prendre en compte :

+ La technique : comprend la mise en place et l’optimisation de titres, la rédaction des balises meta title et description, l’optimisation du temps de chargement…
+ Le contenu : la qualité des textes, leur longueur, la richesse sémantique, la pertinence du contenu par rapport à la requête de l’internaute…
+ La popularité : le nombre et la qualité des liens que reçoit votre site, la popularité de votre marque, l’autorité de votre site…


Les internautes recherchent des millions de mots-clés chaque jour.

Ces requêtes peuvent être de nature très variée et les résultats proposés par les moteurs de recherche se doivent d’être pertinents. Pour se faire, Google and Co mettent sans cesse à jour leurs algorithmes de classement et tentent d’améliorer l’expérience utilisateur.

Mais sachant que les habitudes de recherche des internautes évoluent aussi, les moteurs doivent s’adapter.

La dernière tendance depuis 2018, c’est la recherche vocale.

En effet le graal a longtemps été la première position. Mais maintenant, il est possible d’obtenir ce qu’on appelle la position zéro.

![position 0](dutTCGoogle.png)

Ce type de requête a été déclenchée pour donner suite à une nouvelle façon de rechercher l’information : la recherche vocale.

Quand vous posez une question à votre téléphone. Le contenu de la réponse proposée se trouve dans la position zéro.

Il existe de nombreuses autres façons de travailler le SEO. Par exemple :

+ Le référencement des images : pour que les images d’un site apparaissent dans les résultats « images » des moteurs de recherche.
+ Le référencement local : qui consiste à optimiser une requête pour la recherche d’un point de vente. Un restaurant par exemple.
+ Le référencement des vidéos : qui regroupe les techniques utilisées pour le SEO sur YouTube.

# Black Hat et White Hat

+ Le White Hat SEO regroupe toutes les techniques de référencement naturel qui ne violent pas les guidelines de Google. L’optimisation des balises title et description ou le maillage interne par exemple, sont des techniques qui restent dans la légalité, et qui sont même recommandées par Google.
+ Le Black Hat SEO regroupe des techniques qui sont d’avantage orientées « Growth Hacking » et qui ne se préoccupent pas de respecter les consignes de Google. Parmi ces techniques, on cite souvent le cloacking ou les techniques de liens automatiques.

> Un des premiers et des plus célèbres growth hack vient d’Hotmail. Avant de devenir le site utilisé par des millions d’utilisateurs que l’on connait, Hotmail était un des premiers webmail (lancé en juillet 1996 par Sabeer Bhatia et Jack Smith) avec une faible croissance. Au bout de quelques semaines, les fondateurs se rendent compte que 80% des inscriptions proviennent de recommandations. C’est là que Tim Draper, un des investisseurs, eut l’idée de mettre une phrase à la suite de chaque email :
P.S. I love you. Get your free email at Hotmail. Résultat : la croissance décolle. Au bout de six mois, Hotmail atteint un million d’utilisateurs. En 18 mois, la start-up a 12 millions d’utilisateurs (sur les 70 millions internautes de l’époque) et est vendue à Microsoft pour 400 millions de dollars.

# [Yoast SEO](https://yoast.com/)

Le référencement dans WordPress avec Yoast SEO est une pratique devenue courante dans le monde du web. Bien qu’il ne soit pas le seul, ce plugin gratuit se démarque par sa  simplicité d’utilisation et son aspect intuitif. 

Le but est de faire passer le maximum d’indicateurs au vert. 

Pour ce faire, il faut remplir un certain nombre de critères de lisibilité et des normes de référencement. 

Développé par l’entreprise néerlandaise du même nom, Yoast, Yoast SEO est un logiciel libre qui a fait ses preuves. 

Proposer un bon référencement n’est pas à la porter de tout le monde, et ce genre d’outil permet aux professionnels d’offrir un service de qualité. 

L’extension a déjà été installée des millions de fois et des améliorations et mises à jour sont régulièrement proposées.

# **[Google donne des consignes aux webmasters](https://support.google.com/webmasters/answer/9526064?hl=fr&ref_topic=9456575)**

![projet](https://gitlab.com/dut-tc/internet/raw/master/images/gestiondeprojet.png) **Réaliser un articles qui résume les consignes de SEO données par GOOGLE**
